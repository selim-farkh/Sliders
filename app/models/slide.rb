class Slide < ApplicationRecord

validates :name, presence: true
# has_attached_file :image, styles: { small: "64x64", med: "100x100", large: "200x200" }

  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
end
