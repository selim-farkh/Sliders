json.extract! slide, :id, :name, :created_at, :updated_at, :desc_left, :desc_right
json.url slide_url(slide, format: :json)
